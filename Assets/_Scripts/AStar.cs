using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using Priority_Queue;

public class AStar : MonoBehaviour
{
    [SerializeField] NavNode startNode;
    [SerializeField] NavNode endNode;

    public List<NavNode> GetPath()
    {
        SimplePriorityQueue<NavNode> openSet = new SimplePriorityQueue<NavNode>();
        HashSet<NavNode> closedSet = new HashSet<NavNode>();
        Dictionary<NavNode, NavNode> cameFrom = new Dictionary<NavNode, NavNode>();
        Dictionary<NavNode, float> gScore = new Dictionary<NavNode, float>();

        openSet.Enqueue(startNode, 0f);
        gScore.Add(startNode, 0f);

        NavNode curNode = null;

        while (openSet.Count > 0)
        {
            curNode = openSet.Dequeue();
            if (curNode == endNode) { break; }
            closedSet.Add(curNode);
            for (int i = 0; i < curNode.Edges.Count; i++)
            {
                NavEdge edge = curNode.Edges[i];
                if (closedSet.Contains(edge.ToNode)) { continue; }
                float tempG = gScore[curNode] + edge.Distance;
                float tempF = tempG + GetHeuristic(edge.ToNode);
                if (!openSet.Contains(edge.ToNode))
                {
                    openSet.Enqueue(edge.ToNode, tempF);
                }
                else if (tempG > gScore[edge.ToNode]) { continue; }
                cameFrom[edge.ToNode] = curNode;
                gScore[edge.ToNode] = tempG;
            }
        }
        if(curNode != endNode) { return null; }
        List<NavNode> path = new List<NavNode>();
        while (curNode != startNode)
        {
            path.Add(curNode);
            curNode = cameFrom[curNode];
        }
        path.Add(startNode);
        path.Reverse();
        return path;
    }

    float GetHeuristic(NavNode node)
    {
        return Vector3.Distance(node.transform.position, endNode.transform.position);
    }
}

[CustomEditor(typeof(AStar))]
public class AStarEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        AStar astar = (AStar)target;
        if(GUILayout.Button("Find Path"))
        {
            List<NavNode> path = astar.GetPath();
            if (path == null)
            {
                Debug.Log("A*: No Path found");
            }
            else
            {
                Debug.Log("A* found Path:");
                for (int i = 0; i < path.Count; i++)
                {
                    Debug.Log(path[i].Index);
                }
            }
        }
    }
}
