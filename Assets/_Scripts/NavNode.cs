using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class NavNode : MonoBehaviour
{
    [SerializeField] List<NavEdge> edges = new List<NavEdge>();
    // read only property
    public List<NavEdge> Edges => edges;
    // auto property
    public int Index { get; set; }
}

[System.Serializable]
public class NavEdge
{
    [SerializeField] NavNode fromNode;
    public NavNode FromNode => fromNode;
    [SerializeField] NavNode toNode;
    public NavNode ToNode => toNode;
    public float Distance => Vector3.Distance(fromNode.transform.position, toNode.transform.position);

    public NavEdge(NavNode fromNode, NavNode toNode)
    {
        this.fromNode = fromNode;
        this.toNode = toNode;
    }
}

[CustomEditor(typeof(NavNode))]
public class NavNodeEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        NavNode node = (NavNode)target;
        if (GUILayout.Button("Add Entry"))
        {
            node.Edges.Add(new NavEdge(node, null));
        }
    }
}
