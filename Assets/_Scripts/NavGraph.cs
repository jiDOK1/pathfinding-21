using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class NavGraph : MonoBehaviour
{
    [SerializeField] NavGraphVisualizer visualizer = new NavGraphVisualizer();
    NavNode[] allNodes;
    List<NavEdge>[] adjEdges;

    public void ConstructGraph()
    {
        int numNodes = transform.childCount;
        allNodes = new NavNode[numNodes];
        adjEdges = new List<NavEdge>[numNodes];
        Debug.Log("Constructing Graph!");
        for (int i = 0; i < numNodes; i++)
        {
            NavNode node = transform.GetChild(i).GetComponent<NavNode>();
            node.Index = i;
            node.gameObject.name = $"Node ({i})";
            allNodes[i] = node;
            adjEdges[i] = node.Edges;
        }
    }

    void OnDrawGizmos()
    {
        visualizer.DrawGraph(transform);
    }
}

[System.Serializable]
public class NavGraphVisualizer
{
    [SerializeField] bool doVisualize = false;
    [SerializeField] float gizmoSize = 0.5f;
    [SerializeField] float edgeOffset = 0.1f;
    [SerializeField] float capOffset = 0.1f;
    [SerializeField] float capSize = 0.3f;

    public void DrawGraph(Transform graphTransform)
    {
        if (!doVisualize) { return; }
        foreach (Transform nodeTransform in graphTransform)
        {
            Gizmos.color = Color.red;
            Gizmos.DrawWireCube(nodeTransform.position, Vector3.one * gizmoSize);
            NavNode node = nodeTransform.GetComponent<NavNode>();
            Gizmos.color = Color.green;
            foreach (NavEdge edge in node.Edges)
            {
                if (edge.ToNode == null) { continue; }
                Vector3 toNodePos = edge.ToNode.transform.position;
                Vector3 dir = (toNodePos - nodeTransform.position).normalized;
                Vector3 offset = Vector3.Cross(dir, Vector3.up) * edgeOffset;
                Gizmos.DrawLine(nodeTransform.position + offset, toNodePos - (dir * capOffset) + offset);
                Gizmos.DrawWireCube(toNodePos - (dir * capOffset) + offset, Vector3.one * capSize);
            }
        }
    }
}

[CustomEditor(typeof(NavGraph))]
public class NavGraphEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        NavGraph graph = (NavGraph)target;

        if(GUILayout.Button("Construct NavGraph"))
        {
            graph.ConstructGraph();
        }
    }
}
