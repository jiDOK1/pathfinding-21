using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class BFS : MonoBehaviour
{
    [SerializeField] NavNode startNode;
    [SerializeField] NavNode endNode;

    public List<NavNode> GetPath()
    {
        Dictionary<NavNode, NavNode> cameFrom = new Dictionary<NavNode, NavNode>();
        Queue<NavNode> frontier = new Queue<NavNode>();

        frontier.Enqueue(startNode);
        NavNode curNode = null;

        while (frontier.Count > 0)
        {
            curNode = frontier.Dequeue();
            if (curNode == endNode) { break; }
            foreach (NavEdge edge in curNode.Edges)
            {
                if (!cameFrom.ContainsKey(edge.ToNode))
                {
                    frontier.Enqueue(edge.ToNode);
                    cameFrom.Add(edge.ToNode, curNode);
                }
            }
        }
        if (curNode != endNode) { return null; }
        List<NavNode> route = new List<NavNode>();
        while (curNode != startNode)
        {
            route.Add(curNode);
            curNode = cameFrom[curNode];
        }
        route.Add(startNode);
        route.Reverse();
        return route;
    }
}

[CustomEditor(typeof(BFS))]
public class BFSEditor : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        BFS bfs = (BFS)target;
        if (GUILayout.Button("Find Path"))
        {
            List<NavNode> path = bfs.GetPath();
            if (path == null)
            {
                Debug.Log("No path found!");
                return;
            }
            Debug.Log("BFS found a path:");
            foreach (NavNode node in path)
            {
                Debug.Log(node.Index);
            }
        }
    }
}
